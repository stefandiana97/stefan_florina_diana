package Backand;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OperatieTest {
    @Test
    void adunarePolTest() {
        Polinom pol1=new Polinom("x^2+3x^5-9");
        Polinom pol2=new Polinom("x^4-2x^2+x^3-7");
        Polinom pol3=new Polinom("3x^5+x^4+x^3-x^2-16");
        Operatie o=new Operatie();
        assertEquals(pol3.polinomToString(),o.adunarePol(pol1,pol2).polinomToString());

    }

    @Test
    void scadereTest() {
        Polinom pol1=new Polinom("x^2+3x^5-9");
        Polinom pol2=new Polinom("x^4-2x^2+x^3-7");
        Polinom pol3=new Polinom("3x^5-x^4-x^3+3x^2-2");
        Operatie o=new Operatie();
        assertEquals(pol3.polinomToString(),o.scadere(pol1,pol2).polinomToString());

    }

    @Test
    void inmultireTest() {
        Polinom pol1=new Polinom("x^2+3x^5-9");
        Polinom pol2=new Polinom("x^4-2x^2+x^3-7");
        Polinom pol3=new Polinom("3x^9+3x^8-6x^7+x^6-20x^5-11x^4-9x^3+11x^2+63");
        Operatie o=new Operatie();
        assertEquals(pol3.polinomToString(),o.inmultire(pol1,pol2).polinomToString());

    }

    @Test
    void impartireTest() {
        Polinom pol1 = new Polinom("x^2+3x^5-9");
        Polinom pol2 = new Polinom("x^4-2x^2+x^3-7");
        Polinom rest = new Polinom("9x^3-5x^2+21x-30");
        Polinom cat = new Polinom("3x-3");
        Operatie o = new Operatie();
        o.impartire(pol1, pol2);
        assertEquals(cat.polinomToString(),o.getRezultat().polinomToString());
        assertEquals(rest.polinomToString(), o.getRest().polinomToString());

    }

    @Test
    void derivareTest() {
        Polinom pol1=new Polinom("x^2+3x^5-9");
        Polinom pol3=new Polinom("2x+15x^4");
        Operatie o=new Operatie();
        assertEquals(pol3.polinomToString(),o.derivare(pol1).polinomToString());

    }

    @Test
    void integrareTest() {
        Polinom pol1=new Polinom("x^2+3x^5-9");
        Polinom pol3=new Polinom("0.5x^6+0.33333334x^3-9x");
        Operatie o=new Operatie();
        assertEquals(pol3.polinomToString(),o.integrare(pol1).polinomToString());

    }

    @Test
    void stringToPolinomTest() {
        Polinom pol1=new Polinom("x^2+3x^5-9");
        float coef[]={3,1,-9};
        int putere[]={5,2,0};
        int k=0;
        for(Monom m:pol1.getPolinom())
        {
            assertEquals(coef[k],m.getCoeficient());
            assertEquals(putere[k],m.getPutere());
            k++;
        }
    }

    @Test
    void stringToPolinom2() {
        Polinom pol1=new Polinom("x^2+3^4");
        assertEquals("0",pol1.polinomToString());
        assertEquals(0,pol1.getPolinom().size());
    }

    @Test
    void stringToPolinom3() {
        Polinom pol1=new Polinom("x^2+x^2+x^2");
        float coef[]={3};
        int putere[]={2};
        int k=0;
        for(Monom m:pol1.getPolinom())
        {
            assertEquals(coef[k],m.getCoeficient());
            assertEquals(putere[k],m.getPutere());
            k++;
        }
    }


    @Test
    void stringToPolinom4() {
        Polinom pol1=new Polinom("dfgh");
        assertEquals("0",pol1.polinomToString());
    }

    @Test
    void stringToPolinom5() {
        Polinom pol1=new Polinom("x^3~4");
        assertEquals("0",pol1.polinomToString());

    }

    @Test
    void polinomToString6() {
        Polinom pol1=new Polinom("-7x^3");
        assertEquals("-7x^3",pol1.polinomToString());

    }
}