package Frontand;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {

    private JLabel label1 = new JLabel("Polinom1 ");
    private JLabel label2 = new JLabel("Polinom2 ");
    private JLabel label3 = new JLabel("Rezultat    ");
    private JLabel label4 = new JLabel("Rest          ");
    private JLabel label5 = new JLabel("Introduce-ti un X:   ");
    private JButton buton1 = new JButton("+");
    private JButton buton2 = new JButton("-");
    private JButton buton3 = new JButton("*");
    private JButton buton4 = new JButton("/");
    private JButton buton5 = new JButton("′");
    private JButton buton6 = new JButton("∫");
    private JButton buton8 = new JButton("Evalueaza");
    private JTextField text1 = new JTextField(40);
    private JTextField text2 = new JTextField(40);
    private JTextField text3 = new JTextField(40);
    private JTextField text4 = new JTextField(40);
    private JTextField text5 = new JTextField(12);
    private JLabel obs1 = new JLabel(" ");
    private JLabel obs2 = new JLabel(" ");
    private JLabel obs3 = new JLabel(" ");
    private JButton buton7 = new JButton("Reset");

    public GUI() {
        setSize(800, 500);
        setTitle("Calculator Polinom");

        buton1.setBackground(Color.decode("#ff7481"));
        buton2.setBackground(Color.decode("#ff7481"));
        buton3.setBackground(Color.decode("#ff7481"));
        buton4.setBackground(Color.decode("#ff7481"));
        buton5.setBackground(Color.decode("#ff7481"));
        buton6.setBackground(Color.decode("#ff7481"));
        buton7.setBackground(Color.decode("#ff7481"));
        buton8.setBackground(Color.decode("#ff7481"));
        text1.setBackground(Color.decode("#f5e0ee"));
        text2.setBackground(Color.decode("#f5e0ee"));
        text3.setBackground(Color.decode("#f5e0ee"));
        text4.setBackground(Color.decode("#f5e0ee"));
        text5.setBackground(Color.decode("#f5e0ee"));

        text1.setText("0");
        text2.setText("0");
        text5.setText("0");

        JPanel content0 = new JPanel();
        content0.setBackground(Color.PINK);
        JPanel content = new JPanel();
        content.setBackground(Color.PINK);
        JPanel content1 = new JPanel();
        content1.setBackground(Color.PINK);
        JPanel content2 = new JPanel();
        content2.setBackground(Color.PINK);
        JPanel content3 = new JPanel();
        content3.setBackground(Color.PINK);
        JPanel content4 = new JPanel();
        content4.setBackground(Color.PINK);
        JPanel content5 = new JPanel();
        content5.setBackground(Color.PINK);
        JPanel content6 = new JPanel();
        content6.setBackground(Color.PINK);
        JPanel content8 = new JPanel();
        content8.setBackground(Color.PINK);

        label1.setFont(new Font("Serif", Font.PLAIN,20));
        label2.setFont(new Font("Serif", Font.PLAIN,20));
        label3.setFont(new Font("Serif", Font.PLAIN,20));
        label4.setFont(new Font("Serif", Font.PLAIN,20));
        label5.setFont(new Font("Serif", Font.PLAIN,20));
        buton1.setFont(new Font("Serif", Font.PLAIN,20));
        buton2.setFont(new Font("Serif", Font.PLAIN,20));
        buton3.setFont(new Font("Serif", Font.PLAIN,20));
        buton4.setFont(new Font("Serif", Font.PLAIN,20));
        buton5.setFont(new Font("Serif", Font.PLAIN,20));
        buton6.setFont(new Font("Serif", Font.PLAIN,20));
        buton7.setFont(new Font("Serif", Font.PLAIN,20));
        buton8.setFont(new Font("Serif", Font.PLAIN,20));
        text1.setFont(new Font("Serif", Font.PLAIN,17));
        text2.setFont(new Font("Serif", Font.PLAIN,17));
        text3.setFont(new Font("Serif", Font.PLAIN,17));
        text4.setFont(new Font("Serif", Font.PLAIN,17));
        text5.setFont(new Font("Serif", Font.PLAIN,17));

        content1.add(label1);
        content1.add(text1);
        content1.add(obs1);
        content2.add(label2);
        content2.add(text2);
        content2.add(obs2);
        content3.add(buton1);
        content3.add(buton2);
        content3.add(buton3);
        content3.add(Box.createRigidArea(new Dimension(140,20)));
        content3.add(label5);
        content3.add(text5);
        content4.add(buton4);
        content4.add(buton5);
        content4.add(buton6);
        content4.add(Box.createRigidArea(new Dimension(350,20)));
        content4.add(buton8);
        content5.add(label3);
        content5.add(text3);
        content6.add(label4);
        content6.add(text4);
        content8.add(buton7);
        content8.add(obs3);

        content1.setLayout(new FlowLayout(FlowLayout.LEFT));
        content2.setLayout(new FlowLayout(FlowLayout.LEFT));
        content3.setLayout(new FlowLayout(FlowLayout.LEFT));
        content4.setLayout(new FlowLayout(FlowLayout.LEFT));
        content5.setLayout(new FlowLayout(FlowLayout.LEFT));
        content6.setLayout(new FlowLayout(FlowLayout.LEFT));
        content8.setLayout(new FlowLayout(FlowLayout.LEFT));

        content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
        content.add(Box.createRigidArea(new Dimension(20,20)));
        content.add(content1);
        content.add(content2);
        content.add(content3);
        content.add(content4);
        content.add(content5);
        content.add(content6);
        content.add(content8);

        content0.add(Box.createRigidArea(new Dimension(50,50)));
        content0.add(content);
        content0.setLayout(new BoxLayout(content0, BoxLayout.X_AXIS));
        content0.add(Box.createRigidArea(new Dimension(50,50)));

        setContentPane(content0);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

    }

    public void AdunareListener(ActionListener adunare){buton1.addActionListener(adunare);}
    public void ScadereListener(ActionListener scadere){buton2.addActionListener(scadere);}
    public void InmultireListener(ActionListener inmultire){buton3.addActionListener(inmultire);}
    public void ImpartireListener(ActionListener impartire){buton4.addActionListener(impartire);}
    public void DerivareListener(ActionListener derivare){buton5.addActionListener(derivare);}
    public void IntegrareListener(ActionListener integrare){buton6.addActionListener(integrare);}
    public void ResetListener(ActionListener reset){buton7.addActionListener(reset);}
    public void ValoareListener(ActionListener valoare){buton8.addActionListener(valoare);}

    public String getText1() {
        return text1.getText();
    }

    public String getText2() {
        return text2.getText();
    }

    public String getText5() {
        return text5.getText();
    }

    public void setText1(String text1) {
        this.text1.setText(text1);
    }

    public void setText2(String text2) {
        this.text2.setText(text2);
    }

    public void setText3(String text3) {
        this.text3.setText(text3);
    }

    public void setText4(String text4) {
        this.text4.setText(text4);
    }

    public void setObs1(String obs) {
        this.obs1.setText(obs);
    }

    public void setObs2(String obs) {
        this.obs2.setText(obs);
    }

    public void setObs3(String obs) {
        this.obs3.setText(obs);
    }

    public static void main(String[] args)
    {
        GUI g=new GUI();
        GUIAL gal=new GUIAL(g);
    }
}
