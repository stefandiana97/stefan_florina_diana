package Frontand;

import Backand.Polinom;
import Backand.Operatie;
import java.awt.event.ActionListener;

public class GUIAL {
    private GUI g;

    public GUIAL()
    {
    }

    public GUIAL(GUI g){
        this.g=g;
        g.AdunareListener(new AddListener());
        g.ScadereListener(new SubListener());
        g.InmultireListener(new InmultireListener());
        g.ImpartireListener(new ImpartireListener());
        g.DerivareListener(new DerivareListener());
        g.IntegrareListener(new IntegrareListener());
        g.ResetListener(new ResetListener());
        g.ValoareListener(new ValoareListener());
    }

    class AddListener implements ActionListener {

        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
            ResetListener reset=new ResetListener();
            reset.actionPerformed();
            String t1=g.getText1();
            String t2=g.getText2();
            Polinom p1=new Polinom(t1);
            Polinom p2=new Polinom(t2);
            Operatie o =new Operatie();
            if(p1.isValid() && p2.isValid()) {
                o.adunarePol(p1,p2);
                g.setText3(o.getRezultat().polinomToString());
                g.setText4(o.getRest().polinomToString());
            }
            g.setObs1(p1.getObservatii());
            g.setObs2(p2.getObservatii());
            g.setObs3(o.getRezultat().getObservatii());
        }
    }

    class SubListener implements ActionListener {

        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
            ResetListener reset=new ResetListener();
            reset.actionPerformed();
            String t1=g.getText1();
            String t2=g.getText2();
            Polinom p1=new Polinom(t1);
            Polinom p2=new Polinom(t2);
            Operatie o =new Operatie();
            if(p1.isValid() && p2.isValid()) {
                o.scadere(p1,p2);
                g.setText3(o.getRezultat().polinomToString());
                g.setText4(o.getRest().polinomToString());
            }
            g.setObs1(p1.getObservatii());
            g.setObs2(p2.getObservatii());
            g.setObs3(o.getRezultat().getObservatii());
        }
    }

    class InmultireListener implements ActionListener {

        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
            ResetListener reset=new ResetListener();
            reset.actionPerformed();
            String t1=g.getText1();
            String t2=g.getText2();
            Polinom p1=new Polinom(t1);
            Polinom p2=new Polinom(t2);
            Operatie o =new Operatie();
            if(p1.isValid() && p2.isValid()) {
                o.inmultire(p1,p2);
                g.setText3(o.getRezultat().polinomToString());
                g.setText4(o.getRest().polinomToString());
            }
            g.setObs1(p1.getObservatii());
            g.setObs2(p2.getObservatii());
            g.setObs3(o.getRezultat().getObservatii());
        }
    }

    class ImpartireListener implements ActionListener {

        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
            ResetListener reset=new ResetListener();
            reset.actionPerformed();
            String t1=g.getText1();
            String t2=g.getText2();
            Polinom p1=new Polinom(t1);
            Polinom p2=new Polinom(t2);
            Operatie o =new Operatie();
            if(p1.isValid() && p2.isValid()) {
                o.impartire(p1,p2);
                g.setText3(o.getRezultat().polinomToString());
                g.setText4(o.getRest().polinomToString());
            }
            g.setObs1(p1.getObservatii());
            g.setObs2(p2.getObservatii());
            g.setObs3(o.getRezultat().getObservatii());
        }
    }

    class DerivareListener implements ActionListener {

        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
            ResetListener reset=new ResetListener();
            reset.actionPerformed();
            String t1=g.getText1();
            Polinom p1=new Polinom(t1);
            Operatie o =new Operatie();
            if(p1.isValid()) {
                o.derivare(p1);
                g.setText3(o.getRezultat().polinomToString());
                g.setText4(o.getRest().polinomToString());
            }
            g.setObs1(p1.getObservatii());
            g.setObs3(o.getRezultat().getObservatii());
        }
    }

    class IntegrareListener implements ActionListener {

        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
            ResetListener reset=new ResetListener();
            reset.actionPerformed();
            String t1=g.getText1();
            Polinom p1=new Polinom(t1);
            Operatie o =new Operatie();
            if(p1.isValid()) {
                o.integrare(p1);
                g.setText3(o.getRezultat().polinomToString());
                g.setText4(o.getRest().polinomToString());
            }
            g.setObs1(p1.getObservatii());
            g.setObs3(o.getRezultat().getObservatii());
        }
    }

    class ValoareListener implements ActionListener {

        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
            ResetListener reset = new ResetListener();
            reset.actionPerformed();
            String t1 = g.getText1();
            String t2 = g.getText5();
            String rez;
            try {
                double val = Double.parseDouble(t2);
                Polinom p1 = new Polinom(t1);
                if (p1.isValid()) {
                    rez = String.valueOf(p1.valPolinom(val));
                    g.setText3(rez);
                    g.setText4("0");
                }
                g.setObs1(p1.getObservatii());
            } catch(Exception ex) {
                g.setText3("0");
                g.setText4("0");
                g.setObs3("Invalid");
            }
        }
    }

    class ResetListener implements ActionListener {

        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
            g.setText1("");
            g.setText2("");
            g.setText3("");
            g.setText4("");
            g.setObs1("");
            g.setObs2("");
            g.setObs3("");
        }
        public void actionPerformed() {
            g.setText3("");
            g.setText4("");
            g.setObs1("");
            g.setObs2("");
            g.setObs3("");
        }
    }

}
