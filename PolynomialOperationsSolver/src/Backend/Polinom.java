package Backand;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polinom {

    private ArrayList<Monom> polinom;
    private String OpolinomStr;
    private String polinomStr;
    private static final String variabila="x";
    private String observatii;
    private boolean valid=true;

    public Polinom(){
        this.polinom= new ArrayList<Monom>();
        this.polinomStr="";
        this.observatii="";
    }
    public Polinom(String polinomStr)
    {
        this.polinom=new ArrayList<Monom>();
        this.polinomStr=polinomStr;
        this.observatii="";
        this.stringToPolinom();
        //this.verificaPol();
    }
    public Polinom(Monom m)
    {
        this.polinom = new ArrayList<Monom>();
        this.polinomStr="";
        this.observatii="";
        polinom.add(m);
    }
    public ArrayList<Monom> getPolinom() {
        return polinom;
    }
    public void setPolinom(ArrayList<Monom> polinom) {
        this.polinom = polinom;
    }
    public String getObservatii() {
        return observatii;
    }
    public void setObservatii(String observatii) {
        this.observatii = observatii;
    }

    public boolean isValid() {
        return valid;
    }

    public void addMonom(Monom m){
        polinom.add(m);
    }

    public void remPolinom(Monom m)
    {
        this.polinom.remove(m);
    }

    private void stringToPolinom(){
        OpolinomStr=new String(polinomStr);
        ArrayList<Monom> pol=new ArrayList<>();
        int semn = detSemn(0);
        while (true) {
            Monom m;
            m=newMonom(semn);
            if(m!=null) {
                pol.add(m);
            }
            else {
                break;
            }
            semn=detSemn(1);
            if(semn==0) {
                if (!polinomStr.equals("")) {
                    this.observatii = "invalid";
                    pol = new ArrayList<Monom>();
                    valid = false;
                    break;
                }
            }
        }
        polinom=pol;
        verificaPol();
    }
    private Monom newMonom(int semn)
    {
        Pattern termPattern = Pattern.compile("\\s*([a-zA-Z](\\^\\s*([0-9]+))?)" + "|" + "\\s*([-+]?[0-9]+(\\.[0-9]*)?)[*\\s]?([a-zA-Z](\\^\\s*([0-9]+))?)?");
        Matcher matcher;
        Monom m =new Monom();
        matcher = termPattern.matcher(polinomStr);
        boolean result = matcher.lookingAt();
        if (!result) {
            if (!polinomStr.equals("")) {
                this.observatii = "invalid";
                polinom = new ArrayList<Monom>();
                valid = false;
            }
            return null;
        }
        if (matcher.group(1) != null) {
            m.setCoeficient(semn);
            if (matcher.group(2) == null) {
                m.setPutere(1);
            } else {
                m.setPutere(Integer.valueOf(matcher.group(3)));
            }
        } else {
            m.setCoeficient(semn*Float.valueOf(matcher.group(4)));
            if (matcher.group(6) == null) {
                m.setPutere(0);
            } else if (matcher.group(7) == null) {
                m.setPutere(1);
            } else {
                m.setPutere(Integer.valueOf(matcher.group(8)));
            }
        }
        polinomStr= polinomStr.substring(matcher.end());
        return m;
    }
    private int detSemn(int k)
    {
        Pattern signPattern = Pattern.compile("\\s*([-+])\\s*");
        Matcher matcher;
        int sign = 1;
        matcher=signPattern.matcher(polinomStr);
        if (matcher.lookingAt()) {
            sign=(matcher.group(1).equals("-") ? -1 : 1);
            polinomStr = polinomStr.substring(matcher.end());
            return sign;
        }
        else {
            if(k==0){
                return 1;
            }
            else{
                return 0;
            }
        }
    }

    private void sortD(){
        Collections.sort(this.polinom, new Comparator<Monom>() {
            @Override
            public int compare(Monom o1, Monom o2) {
                if(o1.getPutere()==o2.getPutere())
                    return 0;
                return o1.getPutere()<o2.getPutere()?1:-1;
            }
        });
    }

    protected void duplicateRem()
    {
        for(int i=0;i<polinom.size()-1;i++) {
            while(polinom.get(i).getPutere()==polinom.get(i+1).getPutere()){
                polinom.get(i).setCoeficient(polinom.get(i).getCoeficient()+polinom.get(i+1).getCoeficient());
                polinom.remove(i+1);
                if(i>=polinom.size()-1)
                    break;;
            }
        }
    }

    protected int gradPol()
    {
        int grad=0;
        Monom m;
        Iterator<Monom> it=this.polinom.iterator();
        while(it.hasNext()){
            m=it.next();
            if(grad<m.getPutere())
                grad=m.getPutere();
        }
        return grad;
    }

    protected void elimina(){
        ArrayList<Monom> elimin=new ArrayList<>();
        for(Monom m:polinom) {
            if(m.getCoeficient()==0)
                elimin.add(m);
        }
        for(Monom m:elimin) {
            remPolinom(m);
        }
    }

    protected void verificaPol()
    {
        elimina();
        sortD();
        duplicateRem();
    }

    protected boolean isEmpty()
    {
        if(polinom.size()==0)
            return true;
        else
            return false;
    }

    protected Polinom copyPolinom()
    {
        Polinom c=new Polinom();
        for(Monom m:polinom)
        {
            c.getPolinom().add(m.copyMonom());
        }
        return c;
    }

    public double valPolinom(double x){
        double rez=0;
        for(Monom m:polinom)
        {
            rez+=m.valMonom(x);
        }
        return rez;
    }

    private String pol1ToString(Monom m,int k){
        String rez="";
        int c = (int) m.getCoeficient();
        if (c < 0) {
                if (c != -1) {
                    rez += String.valueOf(c);
                }
                else {
                        if (m.getPutere() == 0) rez += "-1";
                        else rez += "-"; }
                }
        else if (c > 0) {
                if (c != 1) {
                    if(k==0) rez += c;
                    else rez += "+" + c;
                }
                else {
                    if(k==0&&m.getPutere()!=0)
                        rez+= "";
                    else if(k==0&& m.getPutere()==0)
                            rez+= "1";
                         else if(m.getPutere()==0)
                                   rez+="+1";
                              else rez+="+";
                }
        }
        if (m.getPutere() == 1)
            rez += variabila;
        else
            if (m.getPutere() > 0)
            rez += "x^" + m.getPutere();
        return rez;
    }

    private String pol2ToString(Monom m, int k)
    {
        String rez="";
        if (k == 0 || m.getCoeficient() < 0)
            rez += String.valueOf(m.getCoeficient());
        else if (m.getCoeficient() > 0)
            rez += "+" + m.getCoeficient();
        if (m.getPutere() == 1)
            rez += variabila;
        else if (m.getPutere() > 0)
            rez += "x^" + m.getPutere();
        return rez;
    }

    public String polinomToString()
    {
        String rez=new String("");
        verificaPol();
        if(!isEmpty()){
            int k=0;
            for(Monom m:polinom){
                String s1=Float.toString(m.getCoeficient());
                if (s1.substring(s1.indexOf("."),s1.length()).equals(".0")) {
                    rez+=pol1ToString(m,k);
                    k++; }
                else {
                    rez+=pol2ToString(m,k);
                    k++;
                }
            }
        } else
            rez="0";

        return rez;
    }
    public String toString(){
        return ""+polinom;
    }
}
