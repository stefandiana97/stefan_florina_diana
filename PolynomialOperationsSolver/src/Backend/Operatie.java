package Backand;
import java.util.ArrayList;

public class Operatie{

    private Polinom rezultat;
    private Polinom rest;

    public Operatie(Polinom rezultat, Polinom rest) {
        this.rezultat = rezultat;
        this.rest = rest;
    }

    public Operatie() {
        this(new Polinom(),new Polinom("0"));
    }

    public Polinom getRezultat() {
        return rezultat;
    }

    public void setRezultat(Polinom rezultat) {
        this.rezultat = rezultat;
    }

    public Polinom getRest() {
        return rest;
    }

    public void setRest(Polinom rest) {
        this.rest = rest;
    }

    private int binarySearch(ArrayList<Monom> arr, int l, int r, int x)
    {
        if (r>=l){
            int mid = l + (r - l)/2;
            if (arr.get(mid).getPutere() == x)
                return mid;
            if (arr.get(mid).getPutere() < x)
                return binarySearch(arr, l, mid-1, x);
            return binarySearch(arr, mid+1, r, x);
        }
        return -1;
    }

    public Polinom adunarePol(Polinom pol1,Polinom pol2){
        Polinom rez=new Polinom();
        Polinom rest=new Polinom();
        Polinom cpol2=pol2.copyPolinom();
        for(Monom m:pol1.getPolinom()){
            int poz=binarySearch(cpol2.getPolinom(),0,cpol2.getPolinom().size()-1,m.getPutere());
            if(poz!=-1){
                Monom temp = new Monom();
                temp.setCoeficient(m.getCoeficient()+cpol2.getPolinom().get(poz).getCoeficient());
                temp.setPutere(m.getPutere());
                rez.getPolinom().add(temp);
                cpol2.getPolinom().remove(cpol2.getPolinom().get(poz));
            }
            else{
                rez.getPolinom().add(m);
            }
        }
        rez.getPolinom().addAll(cpol2.getPolinom());
        rez.verificaPol();
        this.rezultat=rez;
        this.rest=rest;
        return rez;
    }

    public Polinom scadere(Polinom pol1,Polinom pol2){
        Polinom rez=new Polinom();
        Polinom rest=new Polinom();
        Polinom cpol2=pol2.copyPolinom();
        for(Monom m:pol1.getPolinom()){
            int poz=binarySearch(cpol2.getPolinom(),0,cpol2.getPolinom().size()-1,m.getPutere());
            if(poz!=-1){
                Monom temp = new Monom();
                temp.setCoeficient(m.getCoeficient()-cpol2.getPolinom().get(poz).getCoeficient());
                temp.setPutere(m.getPutere());
                rez.getPolinom().add(temp);
                cpol2.getPolinom().remove(cpol2.getPolinom().get(poz));

            }
            else{
                rez.getPolinom().add(m);
            }
        }
        for(Monom m:cpol2.getPolinom()){
            m.setCoeficient(-m.getCoeficient());
        }
        rez.getPolinom().addAll(cpol2.getPolinom());
        rez.verificaPol();
        this.rezultat=rez;
        this.rest=rest;
        return rez;
    }

    public Polinom inmultire(Polinom pol1,Polinom pol2)
    {
        Polinom rez=new Polinom();
        Polinom rest=new Polinom();
        for(Monom m1:pol1.getPolinom()){
            for(Monom m2:pol2.getPolinom()){
                Monom temp=new Monom();
                temp.setCoeficient(m1.getCoeficient()*m2.getCoeficient());
                temp.setPutere(m1.getPutere()+m2.getPutere());
                rez.getPolinom().add(temp);
            }
        }
        rez.verificaPol();
        this.rezultat=rez;
        this.rest=rest;
        return rez;
    }

    public void impartire(Polinom pol1,Polinom pol2)
    {
        Polinom c=new Polinom();
        Polinom r=pol1.copyPolinom();
        if(pol2.isEmpty()) {
            this.rezultat.setObservatii("Nu se poate face impartirea la 0!");
            return;
        }
        try{
            if(r.gradPol()>=pol2.gradPol()) {
                while (!(r.isEmpty()) && (r.gradPol() >= pol2.gradPol())) {
                    float coef=r.getPolinom().get(0).getCoeficient() / pol2.getPolinom().get(0).getCoeficient();
                    int exp = r.getPolinom().get(0).getPutere() - pol2.getPolinom().get(0).getPutere();
                    Polinom p=new Polinom(new Monom(coef,exp));
                    c=adunarePol(c,p);
                    r=scadere(r,inmultire(p,pol2));
                }
                this.rezultat=c;
                this.rest=r;
            } else {
                pol2.setObservatii("Invalid");
            }
        }
        catch(Exception ex) {
            this.rezultat.setObservatii("Nu se poate face impartirea la 0!");
        }
    }

    public Polinom derivare(Polinom pol)
    {
        Polinom rez=new Polinom();
        Polinom rest=new Polinom();
        for(Monom m:pol.getPolinom()) {
            Monom temp=new Monom();
            if(m.getPutere()!=0) {
                temp.setCoeficient(m.getCoeficient() * m.getPutere());
                temp.setPutere(m.getPutere() - 1);
                rez.getPolinom().add(temp);
            }
        }
        rez.verificaPol();
        this.rezultat=rez;
        this.rest=rest;
        return rez;
    }

    public Polinom integrare(Polinom pol)
    {
        Polinom rez=new Polinom();
        Polinom rest=new Polinom();
        for(Monom m:pol.getPolinom()) {
            Monom temp = new Monom();
            temp.setCoeficient(m.getCoeficient() / (m.getPutere()+1));
            temp.setPutere(m.getPutere() + 1);
            rez.getPolinom().add(temp);
        }
        rez.verificaPol();
        this.rezultat=rez;
        this.rest=rest;
        return rez;
    }

}
