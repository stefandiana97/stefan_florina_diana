package Backand;

public class Monom {

    private float coeficient;
    private int putere;

    public Monom(float coeficient, int putere) {
        this.coeficient=coeficient;
        this.putere = putere;
    }

    public Monom()
    {
        this(0,0);
    }

    public float getCoeficient() {
        return this.coeficient;
    }

    public void setCoeficient(float coeficient) {
        this.coeficient=coeficient;
    }

    public int getPutere() {
        return putere;
    }

    public void setPutere(int putere) {
        this.putere = putere;
    }

    protected Monom copyMonom()
    {
        Monom m=new Monom();
        m.setCoeficient(this.coeficient);
        m.setPutere(this.putere);
        return m;
    }

    protected double valMonom(double x)
    {
       return coeficient*Math.pow(x,putere);
    }

    @Override
    public String toString() {
        return ""+ coeficient + "x^" + putere+" ";
    }

}
